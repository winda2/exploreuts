import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class Information2 extends StatelessWidget{
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(
          Icons.location_city,
          color: Colors.white,
          size: 30,
        ),
        title: Text(
          'More Information',
          style: TextStyle(
            fontSize: 25,
            color: Colors.black87,
            fontWeight: FontWeight.w900,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),

      body: Center(
        child: Column(children: <Widget>[
          Image.asset('assets/savana.jpg', width: 300, height: 200,),
          Text('Tianyar Savana is a meadow that offers stunning views. Here, you will find a view of a vast meadow. You may find these meadows yellowish or green, depending on the season.',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w900,),
          ),
          Text('Address :  Unnamed Road, Tianyar, Kubu, Karangasem Regency, Bali 80853',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w500,),
          ),
        ]
        ),
      ),


      bottomSheet: Container(
        width: double.infinity,
        height: 60,
        child: RaisedButton(
          color: Colors.brown[700],
          child: Text('Back',
            style: TextStyle(fontSize: 21, color: Colors.black87, fontWeight: FontWeight.w900,),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );


  }
}