import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class Information5 extends StatelessWidget{
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(
          Icons.location_city,
          color: Colors.white,
          size: 30,
        ),
        title: Text(
          'More Information',
          style: TextStyle(
            fontSize: 25,
            color: Colors.black87,
            fontWeight: FontWeight.w900,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),

      body: Center(
        child: Column(children: <Widget>[
          Image.asset('assets/tirtagangga.jpg', width: 300, height: 200,),
          Text('Tirta Gangga is a former royal palace in eastern Bali, Indonesia, about 5 kilometres from Karangasem, near Abang.',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w900,),
          ),
          Text('Address :  Jalan Raya Abang Desa Adat, Ababi, Abang, Kabupaten Karangasem, Bali 80852',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w500,),
          ),
        ]
        ),
      ),


      bottomSheet: Container(
        width: double.infinity,
        height: 60,
        child: RaisedButton(
          color: Colors.brown[700],
          child: Text('Back',
            style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.w900,),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );


  }
}