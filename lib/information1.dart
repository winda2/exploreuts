import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class Information1 extends StatelessWidget{
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(
          Icons.location_city,
          color: Colors.white,
          size: 18,
        ),
        title: Text(
          'More Information',
          style: TextStyle(
            fontSize: 18,
            color: Colors.black87,
            fontWeight: FontWeight.w900,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),

      body: Center(
        child: Column(children: <Widget>[
          Image.asset('assets/lempuyang1.jpg', width: 300, height: 200,),
          Text('About Lempuyang'),
          Text('Pura Penataran Agung Lempuyang is a Balinese Hindu temple or pura located in the slope of Mount Lempuyang in Karangasem, Bali.'
              'Pura Penataran Agung Lempuyang is considered as part of a complex of pura surrounding Mount Lempuyang, one of the highly regarded temples of Bali.',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w900,),
          ),
          Text('Address :  Bunutan, Abang, Seraya Bar., Kec. Karangasem, Kabupaten Karangasem, Bali 80852',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w500,),
          ),
        ]
        ),
      ),



      bottomSheet: Container(
        width: double.infinity,
        height: 60,
        child: RaisedButton(
          color: Colors.brown,
          child: Text('Back',
            style: TextStyle(fontSize: 21, color: Colors.black87, fontWeight: FontWeight.w900,),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );


  }
}