import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';


class Information3 extends StatelessWidget{
  Widget build(BuildContext context){
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(
          Icons.location_city,
          color: Colors.white,
          size: 18,
        ),
        title: Text(
          'More Information',
          style: TextStyle(
            fontSize: 25,
            color: Colors.black87,
            fontWeight: FontWeight.w900,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),

      body: Center(
        child: Column(children: <Widget>[
          Image.asset('assets/tamanujung1.jpg', width: 300, height: 200,),
          Text('Ujung Water Palace is a former palace in Karangasem Regency, Bali. Now, this palace also known as Ujung Park or Sukasada Park. It is located approximately 5 kilometres from Amlapura. In the Dutch East Indies era, this place known by the name Waterpaleis. The palace has three large pools. In the middle of the pool, '
              'there is the main building named Gili Bale, connected to the edge of the pool by bridge. ',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w900,),
          ),
          Text('Address :  Karangasem Regency, Bali, Indonesia',
            style: TextStyle(color: Colors.black87, fontSize: 18, fontWeight: FontWeight.w500,),
          ),
        ]
        ),
      ),


      bottomSheet: Container(
        width: double.infinity,
        height: 60,
        child: RaisedButton(
          color: Colors.brown[700],
          child: Text('Back',
            style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.w900,),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );


  }
}