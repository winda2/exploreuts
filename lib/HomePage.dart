import 'package:exploreuts/profile.dart';
import 'package:flutter/material.dart';
import 'package:exploreuts/information1.dart';
import 'package:exploreuts/information2.dart';
import 'package:exploreuts/information3.dart';
import 'package:exploreuts/information4.dart';
import 'package:exploreuts/information5.dart';


class HomePage extends StatelessWidget {
  HomePage({@required this.email, @required this.password});
  final String email;
  final String password;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Explore Karangasem',
            style: TextStyle(
              fontSize: 20,
              color: Colors.black87,
              fontWeight: FontWeight.w900,
            )),
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.home,
          color: Colors.black87,
          size: 30,
        ),
        actions: <Widget>[
          IconButton(
              icon: const Icon(
                Icons.account_circle,
                color: Colors.black87,
                size: 30,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Profile(
                        email: email,
                        password: password,
                      )),
                );
              }),
        ],
        centerTitle: true,
      ),
      body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/lempuyang1.jpg',
              ),
              Text(
                "Let's Explore Karangasem with us !",
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontWeight: FontWeight.w900,
                ),
              ),
              Text(
                'Happy Holiday!',
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),

              Column(children: <Widget>[
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/lempuyang1.jpg',
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        'Pura Lempuyang',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                          color: Colors.black87,
                        ),
                      ),
                      Text(
                        'Bunutan, Abang, Seraya',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87,
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Information1()),
                          );
                        },
                        child: Text(
                          "More",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w800,
                              color: Colors.black87),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/savana.jpg',
                        width: 150,
                        height: 150,
                      ),
                      new Column(
                        children: <Widget>[
                          Text(
                            'Padang Savana Tianyar',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w800,
                                color: Colors.black87),
                          ),
                          Text(
                            'Unnamed Road, Tianyar, Kubu',
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87,
                            ),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Information2()),
                              );
                            },
                            child: Text(
                              "More",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.black87),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/tamanujung1.jpg',
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        ' Taman Ujung',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black87),
                      ),
                      Text(
                        '	Karangasem Regency, Bali, Indonesia',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87,
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Information3()),
                          );
                        },
                        child: Text(
                          "More",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w800,
                              color: Colors.black87),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/tirtagangga.jpg',
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        'Tirta Gangga',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black87),
                      ),
                      Text(
                        'Jalan Raya Abang Desa Adat, Ababi',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87,
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Information4()),
                          );
                        },
                        child: Text(
                          "More",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w800,
                              color: Colors.black87),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/biastugel.jpg',
                        width: 150,
                        height: 150,
                      ),
                      Text(
                        'Bias Tugel Beach',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black87),
                      ),
                      Text(
                        'Padangbai, Manggis, Karangasem',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87,
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Information5()),
                          );
                        },
                        child: Text(
                          "More",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w800,
                              color: Colors.black87),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Text(
                        'A vacation is what you take when you can no longer take what you have been taking.',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black87),
                      ),
                      Text(
                        'Earl Wilson',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w800,
                            color: Colors.black87),
                      ),
                    ],
                  ),
                ),
              ]),
            ],
          )),
      bottomSheet: Container(
        width: double.infinity,
        height: 60,
        child: RaisedButton(
          color: Colors.brown,
          child: Text(
            'Log Out',
            style: TextStyle(
              fontSize: 21,
              color: Colors.black87,
              fontWeight: FontWeight.w900,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
